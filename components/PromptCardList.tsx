import React, { FC } from "react";

import { IPrompt } from "@models/prompt";

import PromptCard from "./PromptCard";

type Props = {
  data: IPrompt[];
  handleTagClick: (tag: string) => void;
};

const PromptCardList: FC<Props> = ({ data, handleTagClick }) => {
  return (
    <div className="mt-16 prompt_layout">
      {data.map((post) => (
        <PromptCard
          key={post._id}
          post={post}
          handleTagClick={handleTagClick}
        />
      ))}
    </div>
  );
};

export default PromptCardList;
