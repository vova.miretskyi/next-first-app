"use client";

import { useState, useEffect } from "react";
import { signIn, signOut, useSession, getProviders } from "next-auth/react";

import Link from "next/link";
import Image from "next/image";

const Nav = () => {
  const { data: session, status } = useSession();
  const [providers, setProviders] = useState<any>(null);
  const [toggleDropdown, setToggleDropdown] = useState(false);
  const isSessionLoading = status === "loading";

  const onSingOutClick = () => {
    signOut();
  };

  const onSingInClick = (id: string) => () => {
    signIn(id);
  };

  const onToggleDropdownClick = () => {
    setToggleDropdown((prevState) => !prevState);
  };

  useEffect(() => {
    (async () => {
      const response = await getProviders();

      setProviders([response]);
    })();
  }, []);

  return (
    <nav className="flex-between w-full mb-16 pt-3">
      <Link href="/" className="flex gap-2 flex-center">
        <Image
          src="/assets/images/logo.svg"
          width={30}
          height={30}
          alt="Promptopia Logo"
          className="object-contain"
        />
        <p className="logo_text">Promptopia</p>
      </Link>

      <div className="sm:flex hidden">
        {session?.user && !isSessionLoading ? (
          <div className="flex gap-3 md:gap-5">
            <Link href="/create-prompt" className="black_btn">
              Create Post
            </Link>
            <button
              type="button"
              onClick={onSingOutClick}
              className="outline_btn"
            >
              Sing out
            </button>

            <Link href="/profile">
              <Image
                loader={() => session?.user?.image || ""}
                src={session?.user.image || ""}
                unoptimized={true}
                width={37}
                height={37}
                className="rounded-full"
                alt="profile"
              />
            </Link>
          </div>
        ) : (
          <>
            {providers &&
              Object.values(
                providers.map((provider: any) => (
                  <button
                    type="button"
                    key={provider.id}
                    onClick={onSingInClick(provider.id)}
                    className="black_btn"
                  >
                    Sign In
                  </button>
                ))
              )}
          </>
        )}
      </div>
      <div className="sm:hidden flex relative">
        {session?.user ? (
          <div className="flex">
            <Image
              loader={() => session?.user?.image || ""}
              src={session?.user.image || ""}
              unoptimized={true}
              width={37}
              height={37}
              className="rounded-full"
              alt="profile"
              onClick={onToggleDropdownClick}
            />
            {toggleDropdown && (
              <div className="dropdown">
                <Link
                  href="/profile"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                  My Profile
                </Link>
                <Link
                  href="/create-prompt"
                  className="dropdown_link"
                  onClick={() => setToggleDropdown(false)}
                >
                  Create Prompt
                </Link>
                <button
                  type="button"
                  onClick={() => {
                    setToggleDropdown(false);
                    onSingOutClick();
                  }}
                  className="mt-5 w-full black_btn"
                >
                  Sing Out
                </button>
              </div>
            )}
          </div>
        ) : (
          <>
            {providers &&
              Object.values(
                providers.map((provider: any) => (
                  <button
                    type="button"
                    key={provider.id}
                    onClick={onSingInClick(provider.id)}
                    className="black_btn"
                  >
                    Sign In
                  </button>
                ))
              )}
          </>
        )}
      </div>
    </nav>
  );
};

export default Nav;
