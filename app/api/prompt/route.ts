import { Prompt } from "@models/prompt";
import { connectToDB } from "@utils/database";

export const GET = async (req: any) => {
  try {
    await connectToDB();

    const data = await Prompt.find({}).populate("creator");

    return new Response(JSON.stringify(data), {
      status: 201,
    });
  } catch (e) {
    throw new Error("/prompt/new POST error: " + e);
  }
};
