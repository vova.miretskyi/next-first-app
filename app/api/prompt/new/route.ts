import { Prompt } from "@models/prompt";
import { connectToDB } from "@utils/database";

export const POST = async (req: any) => {
  const { userId, tag, prompt } = await req.json();

  try {
    await connectToDB();

    const newPrompt = new Prompt({ creator: userId, tag, prompt });
    await newPrompt.save();

    return new Response(JSON.stringify(newPrompt), {
      status: 201,
    });
  } catch (e) {
    throw new Error("/prompt/new POST error: " + e);
  }
};
