import NextAuth from "next-auth/next";
import GoogleProvider from "next-auth/providers/google";

import { connectToDB } from "@utils/database";
import { User } from "@models/user";

const handler = NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID || "",
      clientSecret: process.env.GOOGLE_CLIENT_SECRET || "",
    }),
  ],
  callbacks: {
    async session({ session }: any) {
      const sessionUser = await User.findOne({ email: session.user.email });

      session.user.id = sessionUser._id.toString();

      return session;
    },
    async signIn({ profile }) {
      try {
        if (!profile) throw new Error("Auth error: profile is undefined");

        await connectToDB();

        const userExists = await User.findOne({ email: profile.email });

        if (!userExists && profile) {
          await User.create({
            email: profile.email,
            username: profile?.name?.replace(" ", "").toLowerCase(),
            //@ts-ignore incorrect library type
            image: profile.picture,
          });
        }

        return true;
      } catch (e) {
        console.log("Auth error: ", e);
        return false;
      }
    },
  },
});

export { handler as GET, handler as POST };
